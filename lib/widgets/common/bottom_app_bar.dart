import 'package:flutter/material.dart';
import '../my_flutter_app_icons.dart';

class BottomDocker extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
        color: Color(0xFF0E2D51),
        shape: CircularNotchedRectangle(),
        child: IconTheme(
          data: IconThemeData(color: Color(0xFFFFFFFF), size: 30),
          child: Container(
            height: 75,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Spacer(),
                Column(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        MyFlutterApp.apl_iconset_trophy,
                        color: Color(0xFFFFFFFF),
                      ),
                      alignment: Alignment.centerLeft,
                    ),
                    Text(
                      "Major Event",
                      style: TextStyle(
                        color: Color(0xFFFFFFFF),
                      ),
                    ),
                  ],
                ),
                Spacer(),
                Column(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        MyFlutterApp.apl_iconset_poker1,
                        color: Color(0xFFFFFFFF),
                      ),
                      alignment: Alignment.centerLeft,
                    ),
                    Text(
                      "Leaderboard",
                      style: TextStyle(
                        color: Color(0xFFFFFFFF),
                      ),
                    )
                  ],
                ),
                Spacer()
              ],
            ),
          ),
        ));
  }
}
