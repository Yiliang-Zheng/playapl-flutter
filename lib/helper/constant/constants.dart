library constants;

import 'package:flutter/material.dart';
import 'package:playapl_native/model/model.dart' as model;

 List<model.State> states = [
  model.State(id: 1, name: "VIC"),
  model.State(id: 2, name: "NSW"),
  model.State(id: 3, name: "WA"),
  model.State(id: 4, name: "SA"),
  model.State(id: 5, name: "QLD"),
  model.State(id: 6, name: "TAS")
];

 const Color aplBlue = Color(0xFF0E2D51) ;
 const Color aplGreen = Color(0xFF0BA244);
 const Color aplLightGreen = Color(0xFF99FF33);