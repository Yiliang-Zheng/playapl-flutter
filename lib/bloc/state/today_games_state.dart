import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:playapl_native/model/model.dart';

abstract class TodayGamesState extends Equatable {
  const TodayGamesState();
  @override
  List<Object> get props => [];
}

class TodayGamesLoading extends TodayGamesState {}

class TodayGamesLoaded extends TodayGamesState {
  final List<Event> games;
  final List<State> states;
  final int pageIndex;
  final int selectedWeekday;
  final Map<String, dynamic> filters;
  final bool showFilter;
  TodayGamesLoaded(
      {this.games = const [],
      this.pageIndex = 1,
      this.selectedWeekday = -1,
      this.states = const [],
      this.filters,
      this.showFilter = false});

  @override
  List<Object> get props =>
      [games, pageIndex, selectedWeekday, states, filters, showFilter];
}

class TodayGamesError extends TodayGamesState {
  final String error;
  TodayGamesError({@required this.error}) : assert(error != null);

  @override
  List<Object> get props => [error];
}
