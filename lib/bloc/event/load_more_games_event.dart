import 'package:meta/meta.dart';
import 'package:playapl_native/bloc/event/event_base.dart';

class LoadMoreGamesEvent extends EventBase {
  final int pageIndex;

  LoadMoreGamesEvent({@required this.pageIndex});

  @override
  List<Object> get props => [pageIndex];
}
