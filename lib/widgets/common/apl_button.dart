import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:playapl_native/helper/constant/constants.dart';

class AplButton extends StatelessWidget {
  final String title;
  final Color backgroundColor;
  final VoidCallback onButtonPressed;
  AplButton(
      {@required this.title,
      @required this.onButtonPressed,
      this.backgroundColor = aplLightGreen});
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: this.onButtonPressed,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18),
      ),
      color: this.backgroundColor,
      textColor: aplBlue,
      padding: EdgeInsets.all(10),
      child: Text(
        this.title,
      ),
    );
  }
}
