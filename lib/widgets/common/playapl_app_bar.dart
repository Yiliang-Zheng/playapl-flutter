import 'package:flutter/material.dart';

class PlayAPLAppBar {
  static AppBar getAppBar() {
    return AppBar(
      title: Image.asset(
        "assets/logo.png",
        width: 65,
        height: 65,
      ),
      titleSpacing: 7,
      centerTitle: true,
      backgroundColor: Color(0xFF0E2D51),
    );
  }
}
