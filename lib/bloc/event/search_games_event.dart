import 'package:playapl_native/bloc/event/event_base.dart';

class SearchGamesEvent extends EventBase {
  final int mode;
  final int venueId;
  final int stateId;
  final List<int> regions;
  final DateTime fromDate;
  final DateTime toDate;
  final bool guarantee;
  final int buyInMin;
  final int buyInMax;
  final int brandId;
  final num lat;
  final num lng;

  SearchGamesEvent(
      {this.mode = 1,
      this.venueId,
      this.stateId,
      this.regions = const [],
      this.fromDate,
      this.toDate,
      this.guarantee = false,
      this.buyInMin,
      this.buyInMax,
      this.lat,
      this.lng,
      this.brandId = 2});

  Map<String, dynamic> toJson() => {
        "mode": this.mode,
        "venueId": this.venueId,
        "stateId": this.stateId,
        "regions": this.regions,
        "fromDate": this.fromDate,
        "toDate": this.toDate,
        "guarantee": this.guarantee,
        "buyIn": {"min": this.buyInMin, "max": this.buyInMax},
        "brandId": this.brandId,
        "currentLat": this.lat,
        "currentLng": this.lng,
        "showAllRegions": this.regions.isEmpty ? 0 : 1,
      };

  @override
  List<Object> get props => [
        mode,
        venueId,
        stateId,
        regions,
        fromDate,
        toDate,
        guarantee,
        buyInMin,
        buyInMax,
        lat,
        lng,
        brandId
      ];
}
