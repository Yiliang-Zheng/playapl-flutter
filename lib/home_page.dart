import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:playapl_native/bloc/blocs.dart';
import 'package:playapl_native/bloc/event/change_weekday_event.dart';
import 'package:playapl_native/bloc/event/search_games_event.dart';
import 'package:playapl_native/repositories/games_repository.dart';
import 'package:playapl_native/widgets/common/bottom_app_bar.dart';
import 'package:playapl_native/widgets/common/section_header.dart';
import 'package:playapl_native/widgets/game_list.dart';
import 'package:playapl_native/widgets/major_event_list.dart';
import 'package:playapl_native/widgets/my_flutter_app_icons.dart';
import 'package:playapl_native/widgets/common/playapl_app_bar.dart';

class _HomePageState extends State<HomePage> {
  TodayGamesBloc _todayGamesBloc;
  MajorEventBloc _majorEventBloc;
  SearchFilterBloc _searchFilterBloc;

  @override
  void initState() {
    super.initState();
    this._todayGamesBloc = TodayGamesBloc(gamesRepo: GamesRepository())
      ..add(SearchGamesEvent());
    this._majorEventBloc = MajorEventBloc()..add(SearchMajorEvent());
    this._searchFilterBloc = SearchFilterBloc()..add(LoadFilterItemsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<TodayGamesBloc>(
          create: (context) => this._todayGamesBloc,
        ),
        BlocProvider<MajorEventBloc>(
          create: (context) => this._majorEventBloc,
        ),
        BlocProvider<SearchFilterBloc>(
          create: (context)=>this._searchFilterBloc,
        )
      ],
      child: Scaffold(
        appBar: PlayAPLAppBar.getAppBar(),
        body: Container(
          child: Column(
//            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
             MajorEventList(),
              GameList(
                onWeekdayChange: this.handleWeekdayChange,
              ),
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          child: Icon(
            MyFlutterApp.apl_iconset_date,
            size: 30,
            color: Color(0xFFFFFFFF),
          ),
        ),
        bottomNavigationBar: BottomDocker(),
      ),
    );
  }

  @override
  Future<void> dispose() async {
    super.dispose();
    await _todayGamesBloc.close();
    await _majorEventBloc.close();
  }

  void handleWeekdayChange(int weekday) {
    this._todayGamesBloc.add(ChangeWeekdayEvent(weekday: weekday));
  }
}

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomePageState();
}
