import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:playapl_native/model/major_event.dart';

abstract class MajorEventState extends Equatable{
  @override
  List<Object> get props => [];
}

class MajorEventLoading extends MajorEventState{

}

class MajorEventError extends MajorEventState{
  final String error;
  MajorEventError({@required this.error}):assert(error != null);

  @override
  List<Object> get props =>[error];
}

class MajorEventLoaded extends MajorEventState{
  final List<MajorEvent> majorEvents;
  MajorEventLoaded({this.majorEvents = const []});
}

