import 'package:bloc/bloc.dart';
import 'package:playapl_native/bloc/event/event_base.dart';
import 'package:playapl_native/bloc/event/search_major_event.dart';
import 'package:playapl_native/bloc/state/major_event_state.dart';
import 'package:playapl_native/repositories/major_event_respository.dart';

class MajorEventBloc extends Bloc<EventBase, MajorEventState> {
  MajorEventRepository _majorEventRepo;
  MajorEventBloc() {
    this._majorEventRepo = MajorEventRepository();
  }

  @override
  MajorEventState get initialState => MajorEventLoading();

  @override
  Stream<MajorEventState> mapEventToState(EventBase event) async* {
    if (event is SearchMajorEvent) {
      yield MajorEventLoading();
      try {
        var majorEvents = await this._majorEventRepo.searchMajorEvents(event.state);
        yield MajorEventLoaded(majorEvents: majorEvents);
      } catch (_) {
        yield MajorEventError(error: _);
      }
    }
  }
}
