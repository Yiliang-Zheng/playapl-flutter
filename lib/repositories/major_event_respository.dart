import 'dart:async';

import 'package:playapl_native/model/major_event.dart';
import 'package:playapl_native/repositories/api_client.dart';

class MajorEventRepository {
  ApiClient client;
  MajorEventRepository() {
    client = ApiClient();
  }

  Future<List<MajorEvent>> searchMajorEvents(String state) async {
    var items = (await this.client.getUmbraco("majorEvent/getAll?state=${state ?? "vic"}"))
        as List;
    var result = items
        .map((item) => MajorEvent.fromJson(item as Map<String, dynamic>))
        .toList();
    return result;
  }
}
