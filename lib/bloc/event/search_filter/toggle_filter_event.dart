import 'package:meta/meta.dart';
import '../event_base.dart';

class ToggleFilterEvent extends EventBase {
  final bool showFilter;
  ToggleFilterEvent({@required this.showFilter});
  @override
  List<Object> get props => [showFilter];
}
