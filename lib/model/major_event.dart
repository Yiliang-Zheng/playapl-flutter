import 'package:equatable/equatable.dart';

class MajorEvent extends Equatable {
  final int id;
  final String name;
  final String bannerUrl;
  final String logoUrl;
  final String description;
  final String buyTicketUrl;
  final int linkedMajorEventId;
  final List<String> states;
  final String qualificationProcess;

  MajorEvent(
      {this.id,
      this.name,
      this.bannerUrl,
      this.logoUrl,
      this.description,
      this.buyTicketUrl,
      this.linkedMajorEventId,
      this.states,
      this.qualificationProcess});

  static MajorEvent fromJson(Map<String, dynamic> json) {
    return MajorEvent(
        id: json["id"],
        name: json["name"],
        bannerUrl: json["bannerUrl"],
        logoUrl: json["logoUrl"],
        description: json["description"],
        buyTicketUrl: json["buyTicketUrl"],
        linkedMajorEventId: json["linkedMajorEventId"],
        states: (json["states"] as List).cast<String>().toList(),
        qualificationProcess: json["qualificationProcess"]);
  }

  @override
  List<Object> get props => [
        id,
        name,
        bannerUrl,
        logoUrl,
        description,
        buyTicketUrl,
        linkedMajorEventId,
        states,
        qualificationProcess
      ];
}
