import 'dart:async';
import 'package:playapl_native/bloc/event/search_games_event.dart';
import 'package:playapl_native/model/model.dart';
import 'package:playapl_native/repositories/api_client.dart';

class GamesRepository {
  ApiClient client;
  GamesRepository() {
    this.client = ApiClient();
  }

  Future<List<Event>> searchGames(SearchGamesEvent payload) async {
    var items =
        (await this.client.postApi("event/search", payload.toJson())) as List;
    var result = items
        .map((item) => Event.fromJson(item as Map<String, dynamic>))
        .toList();
    return result;
  }
}
