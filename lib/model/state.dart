import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:playapl_native/model/region.dart';

class State extends Equatable {
  final int id;
  final String name;
  List<Region> _regions;

  State({@required this.id, @required this.name, List<Region> regions}) {
    this._regions = regions;
  }

  List<Region> get regions => this._regions;
  set regions(List<Region> regions) {
    this._regions = regions;
  }

  @override
  List<Object> get props => [id, name, _regions];
}
