import 'package:equatable/equatable.dart';

class UserState extends Equatable {
  final bool isAuthenticated;
  final String accessToken;

  UserState({this.isAuthenticated, this.accessToken});

  @override
  List<Object> get props => [this.accessToken, this.isAuthenticated];
}
