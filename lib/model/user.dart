import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class User extends Equatable {
  final int membershipId;
  final int customerId;
  final String firstName;
  final String lastName;

  User(
      {@required this.membershipId,
      @required this.customerId,
      @required this.firstName,
      @required this.lastName});

  static User fromJson(Map<String, dynamic> json) {
    return User(
        membershipId: json["membership_id"],
        customerId: json["user_id"],
        firstName: json["given_name"],
        lastName: json["family_name"]);
  }

  @override
  List<Object> get props =>
      [this.membershipId, this.customerId, this.firstName, this.lastName];
}
