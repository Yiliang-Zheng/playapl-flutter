import 'event_base.dart';

class SearchMajorEvent extends EventBase {
  final String state;
  SearchMajorEvent({this.state});
  
  @override
  List<Object> get props => [state];
}
