import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:playapl_native/helper/callback/callback.dart';

class WeekdayToggleButton extends StatelessWidget {
  int selectedDay;
  SingleIntVoidCallback onWeekdayPress;
  WeekdayToggleButton({this.selectedDay = -1, @required this.onWeekdayPress});

  @override
  Widget build(BuildContext context) {
    var startTodayWeekdays = Map<int, String>();
    startTodayWeekdays.putIfAbsent(-1, () => "All");
    Iterable<int>.generate(7).forEach((i) {
      var day = DateTime.now().add(Duration(days: i));
      startTodayWeekdays.putIfAbsent(
          day.weekday, () => DateFormat("EEE").format(day));
    });

    return SingleChildScrollView(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: startTodayWeekdays.entries
            .map(
              (item) => Padding(
                padding: EdgeInsets.only(right: 10),
                child: Material(
                  child: InkWell(
                    child: Container(
                      child: Text(
                        item.value,
                        style: TextStyle(fontWeight: FontWeight.bold, color: this.selectedDay == item.key ? Colors.white : Colors.black),
                      ),
                      height: 35,
                      width: 35,
                      padding: EdgeInsets.all(0),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: selectedDay == item.key
                              ? Color(0xFF0BA244)
                              : Colors.transparent),
                    ),
                    onTap: () {
                      this.onWeekdayPress(item.key);
                    },
                    highlightColor: Color(0xFF0BA244),
                    borderRadius: BorderRadius.circular(5),
                  ),
//                  color: Colors.green,
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
