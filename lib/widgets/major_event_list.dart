import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:playapl_native/bloc/blocs.dart';
import 'package:playapl_native/widgets/common/common.dart';
import 'package:playapl_native/widgets/major_event_card.dart';

class MajorEventList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MajorEventBloc, MajorEventState>(
      builder: (context, state) {
        if (state is MajorEventLoading || state is MajorEventError) {
          return EmptyContainer();
        }

        if (state is MajorEventLoaded) {
          if (state.majorEvents.isEmpty) {
            return EmptyContainer();
          }
          return Container(
            margin: EdgeInsets.only(top: 10),
            height: 95,
            child: ListView.builder(
              itemCount: state.majorEvents.length,
              itemBuilder: (context, index) {
                print(index);
                return MajorEventCard(majorEvent: state.majorEvents[index]);
              },
              scrollDirection: Axis.horizontal,
            ),
          );
        }
        return EmptyContainer();
      },
    );
  }
}
