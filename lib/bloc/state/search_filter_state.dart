import 'package:equatable/equatable.dart';
import 'package:playapl_native/model/model.dart';

class SearchFilterState extends Equatable {
  final List<State> states;
  final Map<String, dynamic> selectedFilters;
  final bool showFilter;

  SearchFilterState({this.states, this.selectedFilters = const {}, this.showFilter = false});

  @override
  List<Object> get props => [states, selectedFilters, showFilter];
}
