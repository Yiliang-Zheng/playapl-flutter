import 'package:flutter/material.dart' hide State;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:playapl_native/bloc/blocs.dart';
import 'package:playapl_native/helper/constant/constants.dart';
import 'package:playapl_native/widgets/common/apl_button.dart';
import 'package:playapl_native/widgets/common/common.dart';
import 'package:playapl_native/model/model.dart';

class GameSearchFilter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchFilterBloc, SearchFilterState>(
      builder: (context, state) {
        var selectedStateId = state.selectedFilters["stateId"];
        var selectedRegion = state.selectedFilters["regionId"];
        var regions = state.states
                .firstWhere((element) => element.id == selectedStateId,
                    orElse: () => null)
                ?.regions ??
            [];
        var filterPanel = SizedBox(
          width: double.infinity,
          child: Container(
            color: aplBlue,
            padding: EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "State",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
                Wrap(
                  spacing: 8,
                  runSpacing: 4,
                  children: state.states
                      .map((p) => ChoiceChip(
                            selectedColor: aplGreen,
                            shape: StadiumBorder(
                                side:
                                    BorderSide(color: Colors.white, width: 2)),
                            backgroundColor: aplBlue,
                            label: Text(
                              p.name,
                              style: TextStyle(color: Colors.white),
                            ),
                            selected: p.id == selectedStateId,
                            onSelected: (bool selected) {
                              BlocProvider.of<SearchFilterBloc>(context)
                                ..add(UpdateSearchFilterEvent(
                                    stateId: p.id, regionId: null));
                            },
                          ))
                      .toList(),
                ),
                Text(
                  "Region",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
                DropdownButton<int>(
                  value: selectedRegion,
                  items: regions
                      .map((e) => DropdownMenuItem<int>(
                            value: e.id,
                            child: Text(e.name),
                          ))
                      .toList(),
                  hint: Text(
                    "--Select Region --",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                  onChanged: (int newVal) {
                    BlocProvider.of<SearchFilterBloc>(context)
                      ..add(UpdateSearchFilterEvent(
                          stateId: selectedStateId, regionId: newVal));
                  },
                  isExpanded: true,
                  isDense: true,
                  selectedItemBuilder: (context) {
                    return regions
                        .map<Widget>((e) => Text(
                              e.name,
                              style: TextStyle(color: Colors.white),
                            ))
                        .toList();
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: AplButton(
                    title: "APPLY FILTERS",
                    onButtonPressed: () {
                      var searchEvent = SearchGamesEvent(stateId: selectedStateId, regions: selectedRegion == null ? []: [selectedRegion]);
                      BlocProvider.of<TodayGamesBloc>(context)..add(searchEvent);
                      BlocProvider.of<SearchFilterBloc>(context)..add(ToggleFilterEvent(showFilter: false));
                    },
                  ),
                ),
              ],
            ),
          ),
        );
        return Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SectionHeader(
              title: "Poker Games",
              backgroundColor: aplBlue,
              filterLink: FlatButton(
                child: Text(
                  "Filter",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  BlocProvider.of<SearchFilterBloc>(context)
                      .add(ToggleFilterEvent(showFilter: !state.showFilter));
                },
              ),
            ),
            if (state.showFilter) filterPanel
          ],
        );
      },
    );
  }
}
