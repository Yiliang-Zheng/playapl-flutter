import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:playapl_native/model/model.dart';

class MajorEventCard extends StatelessWidget {
  final MajorEvent majorEvent;

  const MajorEventCard({Key key, @required this.majorEvent})
      : assert(majorEvent != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print("Pressed ${this.majorEvent.name}");
      },
      child: Align(
        child: Container(
          width: 95,
          height: 95,
          margin: EdgeInsets.only(right: 10),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color(0xFF0E2D51), Color(0xFF0D1C2D)]),
            borderRadius: BorderRadius.circular(8),
            image: DecorationImage(
              image: NetworkImage(this.majorEvent.logoUrl),
              fit: BoxFit.contain,
            ),
          ),
        ),
      ),
    );
  }
}
