import 'dart:convert';
import "dart:async";
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import "package:flutter_appauth/flutter_appauth.dart";
import 'package:playapl_native/model/model.dart';

class AuthRepository {
  static const String AUTH_CLIENT_ID = "native.code";
  static const String AUTH_CLIENT_SECRET = "secret";
  static const String AUTH_REDIRECT_URL =
      "io.identityserver.demo:/oauthredirect";
  static const String AUTH_ORIGIN =
      "https://login-dev.fullhousegroup.com.au/identity";
  FlutterAppAuth _appAuth;
  List<String> _scopes;
  AuthorizationServiceConfiguration _serviceConfiguration;
  FlutterSecureStorage _secureStorage;

  AuthRepository() {
    this._appAuth = new FlutterAppAuth();
    this._secureStorage = FlutterSecureStorage();
    this._scopes = <String>[
      'openid',
      'profile',
      'email',
      'offline_access',
      'api'
    ];
    this._serviceConfiguration = AuthorizationServiceConfiguration(
        '$AUTH_ORIGIN/connect/authorize', '$AUTH_ORIGIN/connect/token');
  }

  Future<void> signIn() async {
    try {
      final AuthorizationTokenRequest request = AuthorizationTokenRequest(
          AUTH_CLIENT_ID, AUTH_REDIRECT_URL,
          clientSecret: AUTH_CLIENT_SECRET,
          scopes: this._scopes,
          serviceConfiguration: this._serviceConfiguration);
      final AuthorizationTokenResponse result =
          await this._appAuth.authorizeAndExchangeCode(request);
      await this._setToken(result);
    } catch (e) {}
  }

  Future<void> signOut() async {
    try {
      await this._secureStorage.delete(key: "access_token");
      await this._secureStorage.delete(key: "refresh_token");
    } catch (e) {}
  }

  Future<String> getAccessToken() async {
    var accessTokenStr = await this._secureStorage.read(key: "access_token");
    if (accessTokenStr == null) return null;

    Map<String, String> accessToken = jsonDecode(accessTokenStr);
    var expiredAt = DateTime.tryParse(accessToken["expired_at"]);
    var token = accessToken["token"];
    if (token.isNotEmpty &&
        expiredAt != null &&
        expiredAt.isAfter(DateTime.now())) return token;

    var refreshToken = await this._secureStorage.read(key: "refresh_token");
    if (refreshToken?.isEmpty ?? true) return null;

    final TokenResponse result = await _appAuth.token(TokenRequest(
        AUTH_CLIENT_ID, AUTH_REDIRECT_URL,
        refreshToken: refreshToken,
        clientSecret: AUTH_CLIENT_SECRET,
        serviceConfiguration: this._serviceConfiguration,
        scopes: this._scopes));

    if (result != null) {
      await this._setToken(result);
      return result.accessToken;
    }
    return null;
  }

  Future<User> getUserDetails(String accessToken) async {
    final url = "$AUTH_ORIGIN/connect/userinfo";
    final response = await http.get(
      url,
      headers: {"Authorization": "Bearer $accessToken"},
    );

    if (response.statusCode == 200) {
      var json =  jsonDecode(response.body);
      return User.fromJson(json);
    } else {
      throw Exception('Failed to get user details');
    }
  }

  Future<void> _setToken(dynamic response) async {
    await _secureStorage.write(
        key: "access_token",
        value: jsonEncode({
          "token": response.accessToken,
          "expired_at":
              response.accessTokenExpirationDateTime?.toIso8601String()
        }));
    await this
        ._secureStorage
        .write(key: "refresh_token", value: response.refreshToken);
  }
}
