import 'dart:convert';
import "dart:async";
import "package:http/http.dart" as http;

class ApiClient {
  static const baseUrl = "https://api.fullhousegroup.com.au";
  static const baseUmbracoUrl = "https://playapl.com/umbraco/api";
  http.Client httpClient;

  ApiClient() {
    httpClient = http.Client();
  }

  Future<dynamic> getApi(String action) async {
    final url = "$baseUrl/$action";
    var result = await _get(url);
    return result;
  }

  Future<dynamic> getUmbraco(String action) async {
    final url = "$baseUmbracoUrl/$action";
    var result = await _get(url);
    return result;
  }

  Future<dynamic> postApi(String action, Map<String, dynamic> payload) async {
    final url = "$baseUrl/$action";
    var result = await this._post(url, jsonEncode(payload));
    return result;
  }

  Future<dynamic> _get(String url) async {
    final response = await this.httpClient.get(url, headers: {
      "Content-Type": "application/json",
      "Accept": "application/json"
    });
    if (response.statusCode != 200) {
      throw Exception("error from request: $url, reason: ${response.body}");
    }

    var result = jsonDecode(response.body);
    return result;
  }

  Future<dynamic> _post(String url, String payload) async{
    final response = await this.httpClient.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body : payload);

    if (response.statusCode != 200) {
      throw Exception("error from request: $url, reason: ${response.body}");
    }

    var result = jsonDecode(response.body);
    return result;
  }
}
