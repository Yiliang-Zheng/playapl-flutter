import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

class Region extends Equatable {
  final int id;
  final String name;
  final int stateId;

  const Region(
      {@required this.id, @required this.name, @required this.stateId});

  static Region fromJson(Map<String, dynamic> json) {
    var state = json["state"] as Map<String, dynamic>;
    return Region(
        id: json["id"],
        name: json["name"],
        stateId: state == null ? 0 : state["id"]);
  }

  @override
  List<Object> get props => [id, name, stateId];
}
