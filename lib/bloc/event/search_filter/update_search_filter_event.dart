import '../event_base.dart';

class UpdateSearchFilterEvent extends EventBase{
  final int stateId;
  final int regionId;

  UpdateSearchFilterEvent({this.stateId, this.regionId});
  @override
  List<Object> get props => [stateId, regionId];

}