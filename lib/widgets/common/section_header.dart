import 'package:meta/meta.dart';

import 'package:flutter/material.dart';

class SectionHeader extends StatelessWidget {
  String title;
  Color backgroundColor;
  Widget filterLink;

  SectionHeader(
      {@required this.title,
      this.backgroundColor = const Color(0xFF0E2D51),
      this.filterLink})
      : assert(title != null);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      margin: EdgeInsets.only(top: 15),
      padding: EdgeInsets.all(10),
      color: this.backgroundColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Text(
              this.title,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          this.filterLink
        ].where((o) => o != null).toList(),
      ),
    );
  }
}
