import 'package:equatable/equatable.dart';

class Event extends Equatable {
  final int id;
  final String title;
  final bool isFavourite;
  final String description;
  final num buyInAmount;
  final DateTime dateTime;
  final DateTime eventEndDate;
  final String startTime;
  final String type;
  final num takeHomeAmount;
  final String rego;

  Event(
      this.id,
      this.title,
      this.isFavourite,
      this.description,
      this.buyInAmount,
      this.dateTime,
      this.eventEndDate,
      this.startTime,
      this.type,
      this.takeHomeAmount,
      this.rego);

  static Event fromJson(Map json) {
    Map<String, dynamic> venue = json["venue"];
    return Event(
        json["eventId"],
        venue == null ? json["eventName"] : venue["name"],
        false,
        json["eventName"],
        json["playerEntryFee"],
        DateTime.tryParse(json["eventDate"]),
        DateTime.tryParse(json["eventEndDate"] ?? ""),
        json["startTime"],
        json["divisionName"],
        json["guarantee"],
        json["registrationTime"]);
  }

  @override
  List<Object> get props => [
    id,
    title,
    description,
    isFavourite,
    dateTime,
    eventEndDate,
    startTime,
    type,
    takeHomeAmount,
    rego
  ];
}
