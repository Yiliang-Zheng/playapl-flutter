import "dart:async";

import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:playapl_native/bloc/blocs.dart';
import 'package:playapl_native/repositories/games_repository.dart';

class TodayGamesBloc extends Bloc<EventBase, TodayGamesState> {
  final GamesRepository gamesRepo;
  TodayGamesBloc({@required this.gamesRepo});
  @override
  TodayGamesState get initialState => TodayGamesLoading();

  @override
  Stream<TodayGamesState> mapEventToState(EventBase event) async* {
    final currentState = state;
    if (event is SearchGamesEvent) {
      yield TodayGamesLoading();
      try {
        print("Start Search");
        var games = await this.gamesRepo.searchGames(event);
        print("Finish Search");
        yield TodayGamesLoaded(
            pageIndex: 1,
            games: games,
            selectedWeekday: DateTime.now().weekday);
      } catch (_) {
        yield TodayGamesError(error: _);
      }
    } else if (event is LoadMoreGamesEvent) {
      if (currentState is TodayGamesLoaded) {
        yield TodayGamesLoaded(
            selectedWeekday: currentState.selectedWeekday,
            games: currentState.games,
            pageIndex: event.pageIndex);
      }
    } else if (event is ChangeWeekdayEvent) {
      if (currentState is TodayGamesLoaded) {
        yield TodayGamesLoaded(
            selectedWeekday: event.weekday,
            games: currentState.games,
            pageIndex: 1);
      }
    } else if (event is ToggleFilterEvent) {
      if (currentState is TodayGamesLoaded) {
        yield TodayGamesLoaded(
          games: currentState.games,
          showFilter: event.showFilter,
          filters: currentState.filters,
          pageIndex: currentState.pageIndex,
          selectedWeekday: currentState.selectedWeekday,
          states: currentState.states
        );
      }
    }
  }
}
