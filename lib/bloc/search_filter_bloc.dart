import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:playapl_native/bloc/blocs.dart';
import 'package:playapl_native/repositories/repositories.dart';
import 'package:playapl_native/helper/constant/constants.dart' as Constants;

class SearchFilterBloc extends Bloc<EventBase, SearchFilterState>{
  ItemsRepository _item;
  SearchFilterBloc(){
    this._item = ItemsRepository();
  }

  @override
  SearchFilterState get initialState => SearchFilterState(states: Constants.states, showFilter: false);

  @override
  Stream<SearchFilterState> mapEventToState(EventBase event) async* {
    if(event is ToggleFilterEvent){
      yield SearchFilterState(states: state.states, selectedFilters: state.selectedFilters, showFilter: event.showFilter);
    }else if(event is LoadFilterItemsEvent){
      var items = await this._item.getStates();
      yield SearchFilterState(states: items, selectedFilters: state.selectedFilters, showFilter: state.showFilter);
    }else if (event is UpdateSearchFilterEvent){
      yield SearchFilterState(states: state.states, selectedFilters: {...state.selectedFilters, "stateId": event.stateId, "regionId" : event.regionId}, showFilter: state.showFilter);
    }
  }
}