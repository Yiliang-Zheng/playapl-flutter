import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:playapl_native/model/event.dart';

class GameCard extends StatelessWidget {
  final Event _event;
  GameCard(
    this._event, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var dateFormatter = DateFormat("E,d MMM");
    if (this._event == null) return null;

    return Container(
      decoration: BoxDecoration(
        gradient: new LinearGradient(
            stops: [0.02, 0.02], colors: [Color(0xFF0E2D51), Colors.white]),
        borderRadius: BorderRadius.all(Radius.circular(6.0)),
        color: Color(0xFFFFFFFF),
      ),
      padding: EdgeInsets.only(left: 9.0, top: 4.0, right: 9.0, bottom: 4.0),
      margin: EdgeInsets.all(7.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: Text(
              this._event.title,
              style: TextStyle(
                  color: Color(0xFF0BA244),
                  fontSize: 14,
                  fontWeight: FontWeight.w700),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 10, left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Text(
                    this._event.description,
                    style: TextStyle(
                      color: Color(0xFF686D73),
                      fontSize: 12,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                RichText(
                  text: TextSpan(
                    text: "Buy In ",
                    style: TextStyle(
                      color: Color(0xFF686D73),
                      fontSize: 12,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: this._event.buyInAmount == null ||
                                this._event.buyInAmount <= 0
                            ? "Free"
                            : "\$${this._event.buyInAmount.toStringAsFixed(0)}",
                        style: TextStyle(
                          color: Color(0xFF0D1C2D),
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "${dateFormatter.format(this._event.dateTime)} - ${this._event.startTime}",
                  style: TextStyle(color: Color(0xFF686D73), fontSize: 12),
                ),
                (_event.takeHomeAmount ?? 0) > 0
                    ? Container(
                        decoration: BoxDecoration(
                          color: Color(0xFF0BA244),
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                        ),
                        padding: EdgeInsets.only(
                            left: 2, top: 2, bottom: 2, right: 5),
                        height: 19.0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.white, shape: BoxShape.circle),
                              width: 15,
                              height: 15,
                              margin: EdgeInsets.only(right: 5),
                              child: Text(
                                "G",
                                style: TextStyle(
                                  color: Color(0xFF0BA244),
                                  fontSize: 12,
//                            fontWeight: FontWeight.bold,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Text(
                              "${NumberFormat.simpleCurrency(locale:"en-AU", decimalDigits: 0).format(_event.takeHomeAmount)}",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 10),
                            ),
                          ],
                        ),
                      )
                    : null
              ].where((o) => o != null).toList(),
            ),
          )
        ],
      ),
    );
  }
}
