import 'package:meta/meta.dart';
import 'event_base.dart';

class ChangeWeekdayEvent extends EventBase {
  int weekday;
  ChangeWeekdayEvent({@required this.weekday});

  @override
  List<Object> get props => [weekday];
}
