import 'package:bloc/bloc.dart';
import 'package:playapl_native/bloc/blocs.dart';
import 'package:playapl_native/bloc/event/user/fetch_user_access_token_event.dart';
import 'package:playapl_native/bloc/state/user_state.dart';
import 'package:playapl_native/repositories/repositories.dart';

class UserBloc extends Bloc<EventBase, UserState> {
  AuthRepository _authRepo;
  UserBloc() {
    _authRepo = AuthRepository();
  }
  @override
  UserState get initialState =>
      UserState(isAuthenticated: false, accessToken: null);

  @override
  Stream<UserState> mapEventToState(EventBase event) async* {
    if (event is FetchUserAccessTokenEvent) {
      var accessToken = await this._authRepo.getAccessToken();
      if (accessToken == null || accessToken.isEmpty)
        yield UserState(isAuthenticated: false, accessToken: accessToken);
      yield UserState(isAuthenticated: true, accessToken: accessToken);
    }
  }
}
