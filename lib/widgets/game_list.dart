import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:playapl_native/bloc/blocs.dart';
import 'package:playapl_native/bloc/event/load_more_games_event.dart';
import 'package:playapl_native/bloc/state/today_games_state.dart';
import 'package:playapl_native/helper/callback/callback.dart';
import 'package:playapl_native/model/event.dart';
import 'package:playapl_native/widgets/common/section_header.dart';
import 'package:playapl_native/widgets/game_card.dart';
import 'package:playapl_native/widgets/game_search_filter.dart';
import 'package:playapl_native/widgets/weekday_toggle_button.dart';

class GameList extends StatelessWidget {
  SingleIntVoidCallback onWeekdayChange;
  GameList({@required this.onWeekdayChange});
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TodayGamesBloc, TodayGamesState>(
      // ignore: missing_return
      builder: (context, state) {
        if (state is TodayGamesLoading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        if (state is TodayGamesError) {
          return Center(
            child: Text(state.error),
          );
        }

        if (state is TodayGamesLoaded) {
          if (state.games.isEmpty) {
            return Center(
              child: Text("No games found"),
            );
          }

          var items = state.games;
          if (state.selectedWeekday > -1) {
            items = state.games
                .where((p) => p.dateTime.weekday == state.selectedWeekday)
                .toList();
          }
          items = items.sublist(0, state.pageIndex * 10);

          return Container(
            child: Expanded(
              child: Column(
                children: <Widget>[
                  GameSearchFilter(),
                  WeekdayToggleButton(
                    selectedDay: state.selectedWeekday,
                    onWeekdayPress: this.onWeekdayChange,
                  ),
                  Flexible(
                    child: ListView.builder(
                      itemCount: items.length,
                      itemBuilder: (context, index) {
                        if (index + 1 == items.length) {
                          return RaisedButton(
                            onPressed: () {
                              var bloc = context.bloc<TodayGamesBloc>();
                              bloc.add(LoadMoreGamesEvent(
                                pageIndex: state.pageIndex + 1,
                              ));
                            },
                            child: Text("Load More"),
                          );
                        } else {
                          return GameCard(items[index]);
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        }
      },
    );
  }
}
