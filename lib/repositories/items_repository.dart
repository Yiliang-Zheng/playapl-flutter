import 'dart:async';
import 'package:playapl_native/repositories/base_repository.dart';
import 'package:playapl_native/model/model.dart';
import 'package:playapl_native/helper/constant/constants.dart' as Constants;

class ItemsRepository extends BaseRepository{
  ItemsRepository():super();
  Future<List<State>> getStates() async {
    var items = (await this.client.getUmbraco("region/list")) as List;
    var regions = items
        .map((item) => Region.fromJson(item as Map<String, dynamic>))
        .toList();
    var states = Constants.states;
    states.forEach((state) {
      state.regions = regions.where((p) => p.stateId == state.id).toList();
    });
    return states;
  }
}